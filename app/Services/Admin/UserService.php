<?php

namespace App\Services\Admin;

use App\Models\User;
use App\Services\BaseService;
use App\Transformers\TestTranformer;

class UserService extends BaseService
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get list user register and contact;
     *
     * @return Responder
     * @param $param
     */
    public function getListUser($param)
    {

    }

    /**
     * @param $user
     * @return Responder
     */
    public function showUser($user)
    {
        $user = $this->user->where('id', $user)->first();
        return responder()->data($user, new TestTranformer());
    }

    /**
     * @param array $data
     * @return Responder
     */
    public function updateStatus($data)
    {
        $user = $this->user->where('id', $data['id'])->first();
        $admin = $this->user->where('id', $data['user_id'])->first();
        $data = $user->status->update([
            'support_status' => $data['status'],
            'support_datetime' => now(),
            'support_users' => $admin->admin_user->name,
        ]);
        return responder()->updated();
    }

    /**
     * @param array $data
     * @return Responder
     */
    public function updateInforUser($data)
    {
        $user = $this->user->where('id', $data['id'])->first();
        if ($user) {
            $user->update(['email' => $data['email']]);
            // Update db if company
            if ($user->status->user_type == $this->contactApplication::USER_TYPE_COMPANY) {
                $user->company_user->update([
                    'name' => $data['manager_name'],
                ]);
                $user->company_user->company->update([
                    'name' => $data['name'],
                    'phone_number' => $data['phone_number'],
                    'url' => $data['home_page'],
                ]);
                $user->status->update([
                    'consideration_status' => $data['consideration_status'],
                    'question_answer' => $data['question_answers'],
                    'inquiry' => $data['inquiry'],
                ]);
            }
            // Update db if recruit
            if ($user->status->user_type == $this->contactApplication::USER_TYPE_RECRUIT) {
                $user->recruit_user->company->update([
                    'name' => $data['name'],
                    'manager_name' => $data['manager_name'],
                    'phone_number' => $data['phone_number'],
                    'home_page' => $data['home_page'],
                ]);
                $user->status->update([
                    'consideration_status' => $data['consideration_status'],
                    'question_answer' => $data['question_answers'],
                    'inquiry' => $data['inquiry'],
                ]);
            }
            // Update db if outsource
            if ($user->status->user_type == $this->contactApplication::USER_TYPE_OUTSOURCE) {
                $user->outsource_user->company->update([
                    'name' => $data['name'],
                    'manager_name' => $data['manager_name'],
                    'phone_number' => $data['phone_number'],
                    'home_page' => $data['home_page'],
                ]);
                $user->status->update([
                    'consideration_status' => $data['consideration_status'],
                    'question_answer' => $data['question_answers'],
                    'inquiry' => $data['inquiry'],
                ]);
            }
        }

        return responder()->updated();
    }

    /**
     * @param array $data
     * @return Responder
     */
    public function updateMemo($data)
    {
        $user = $this->user->where('id', $data['id'])->first();
        $user->status->update(['admin_memo' => $data['status']]);

        return responder()->updated();
    }

    /**
     * @param Int $id
     */
    public function updateCompany($id)
    {
       $user = $this->user->where('id', $id)->first();
            if($user->user_type_id == 1 && $user->company_user->company != null) {
                $user->company_user->company->update(['status' => 2]);
            }
            elseif($user->user_type_id == 2 && $user->recruit_user->company != null ) {
                $user->recruit_user->company->update(['status' => 2]);
            }
            elseif($user->user_type_id == 3 && $user->outsource_user->company != null) {
                $user->outsource_user->company->update(['status' => 2]);
            }
        return responder()->updated();
    }

}
