<?php

namespace App\Transformers;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use League\Fractal\TransformerAbstract;

class TestTranformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user): array
    {
        return [
            'id' => $user->id,
            'email' => $user->email,
        ];
    }

    // /**
    //  * @param RecruitOfferInfo $recruitOfferInfo
    //  * @return Collection
    //  */
    // public function includeRecruitCompanyUser(Model $Model)
    // {
    //     return $this->collection($recruitInterview->recruitCompanyUser(), new RecruitCompanyUserTransformer());
    // }
   

    // nếu muốn include 1 thêm thì viết vaod đay 
    
}
