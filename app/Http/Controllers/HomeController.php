<?php

namespace App\Http\Controllers;

use App\Services\Admin\UserService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public $userService;
    
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(){
        return  $this->userService->showUser(1);
    }

}
