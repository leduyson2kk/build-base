<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

if (!function_exists('appName')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function appName()
    {
        return config('app.name', 'InCul');
    }
}

if (!function_exists('carbon')) {
    /**
     * Create a new Carbon instance
     *
     * @return Carbon   
     */
    function carbon()
    {
        return new Carbon();
    }
}

if (!function_exists('option')) {
    /**
     * Create a new OptionUtility instance
     *
     * @return \App\Utils\OptionUtility
     */
    function option()
    {
        return app('option');
    }
}

if (!function_exists('responder')) {
    /**
     * Get custom response utility
     *
     * @return \App\Utils\ResponseUtility
     */
    function responder()
    {
        return app('responder');
    }
}

if (!function_exists('getWebURL')) {
    /**
     * Build web url with query params
     *
     * @param string $segment
     * @param array $queryParams
     * @return string
     */
    function getWebURL(string $segment, array $queryParams = [])
    {
        $queryString = http_build_query($queryParams);
        return config('const.web_url') . $segment . ($queryString ? '?' . http_build_query($queryParams) : '');
    }
}

if (!function_exists('getStoragePath')) {
    /**
     * @param $path
     * @return string
     */
    function getStoragePath($path): string
    {
        $parseRequestPath = optional(parse_url($path));
        $parseStorageURL = optional(parse_url(Storage::url('/')));
        $path = ($parseStorageURL['host'] === $parseRequestPath['host']) ? $parseRequestPath['path'] : $path;

        return trim($path, '/');
    }
}

if (!function_exists('getStorageURL')) {
    /**
     * @param $path
     * @return string
     */
    function getStorageURL($path): string
    {
        $parsePath = optional(parse_url($path));
        if ($parsePath['host']) {
            return $path;
        }

        return Storage::url($path);
    }
}

if (!function_exists('mb_trim')) {
    /**
     * @param $str
     * @return 
     */
    function mb_trim($str, $chars = '\s　')
    {
        $str = preg_replace("/^[$chars]+/u", '', $str);
        $str = preg_replace("/[$chars]+$/u", '', $str);

        return $str;
    }
}
