<?php

return [
    // General
    'error_message' =>  'An error has occurred.',
    'rate_limit_request' => env('RATE_LIMIT_REQUEST', 100),
    'default_string_length' => 191,
    'default_country_id' => 1,
    'default_date_format' => 'Y.m.d',
    'default_date_time_format' => 'Y/m/d h:i:s',
    'default_limit' => 10,
    'offer_info_limit' => 50,
    'response_success' => 'success',
    'activated_status' => 1,
    'max_width_image_upload' => 1000,
    'web_url' => env('WEB_URL'),
    'password_reset_expires_in' => 1, // hour
    'default_contact_type_user' => 1,
    'default_support_status_chat' => 1,
    'default_read_flg_chat' => 1,

    // Web path for mail
    'web_path' => [
        'reset_password' => env('WEB_PASSWORD_RESET_PATH'),
        'confirmation' => env('WEB_CONFIRMATION_PATH')
    ],

    // Https api get ip address
    'api_get_ip' => 'https://api.ipify.org/?format=json',

    // Regex pattern rules
    'rule' => [
        'email' => 'regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
        'phone' => 'regex:/(02|03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/',
        'password' => 'regex:/(?=^.{8,16}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', // 8 - 16 characters, have at least 1 capital letter and 1 special character
        'date' => 'date_format:d/m/Y',
        'limit' => 'nullable|integer|min:1|max:200',
        'url' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        'route' => 'regex:/^(\d+|p-\S+|index)$/',
    ]
];
